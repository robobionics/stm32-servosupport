/*
 * trajectories.h
 *
 *  Created on: Sep 11, 2017
 *      Author: victor
 */

#ifndef TRAJECTORIES_HPP_
#define TRAJECTORIES_HPP_

// Trajectories
float IndexOpen2Tip[] = {110.0, 109.55, 108.27, 106.22, 103.48, 100.12, 96.21, 91.82, 87.04, 81.92, 76.56, 71.0, 65.34, 59.65, 53.99, 48.44, 43.07, 37.95, 33.17, 28.78, 24.88, 21.51, 18.77, 16.72, 15.44};

float ThumbOpen2Tip[] = {15.0, 15.39, 16.54, 18.37, 20.83, 23.84, 27.33, 31.26, 35.54, 40.11, 44.91, 49.88, 54.95, 60.04, 65.11, 70.07, 74.88, 79.45, 83.73, 87.66, 91.15, 94.16, 96.62, 98.45, 99.6};

float OtherOpen2Tip[] = {180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0};

float IndexOpen2Power[] = {110.0, 109.48, 108.0, 105.62, 102.45, 98.56, 94.03, 88.95, 83.41, 77.49, 71.28, 64.85, 58.29, 51.7, 45.14, 38.72, 32.5, 26.58, 21.04, 15.96, 11.43, 7.54, 4.37, 1.99, 0.51};

float ThumbOpen2Power[] = {15.0, 15.16, 15.63, 16.39, 17.4, 18.64, 20.08, 21.69, 23.45, 25.34, 27.31, 29.36, 31.45, 33.54, 35.63, 37.67, 39.65, 41.54, 43.3, 44.91, 46.35, 47.59, 48.6, 49.36, 49.83};

float OtherOpen2Power[] = {180.0, 179.39, 177.63, 174.83, 171.08, 166.48, 161.13, 155.13, 148.58, 141.58, 134.24, 126.64, 118.89, 111.1, 103.35, 95.76, 88.41, 81.41, 74.86, 68.86, 63.52, 58.91, 55.16, 52.36, 50.6};

float IndexTip2Open[] = {15.0, 15.44, 16.72, 18.77, 21.51, 24.88, 28.78, 33.17, 37.95, 43.07, 48.44, 53.99, 59.65, 65.34, 71.0, 76.56, 81.92, 87.04, 91.82, 96.21, 100.11, 103.48, 106.22, 108.27, 109.55};

float ThumbTip2Open[] = {100.0, 99.6, 98.45, 96.62, 94.16, 91.16, 87.66, 83.73, 79.45, 74.88, 70.08, 65.11, 60.04, 54.95, 49.88, 44.92, 40.11, 35.54, 31.26, 27.33, 23.84, 20.83, 18.37, 16.54, 15.39};

float OtherTip2Open[] = {180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0, 180.0};

float IndexTip2Power[] = {15.0, 14.92, 14.72, 14.4, 13.97, 13.44, 12.82, 12.13, 11.37, 10.56, 9.71, 8.84, 7.94, 7.05, 6.15, 5.28, 4.43, 3.62, 2.86, 2.17, 1.56, 1.02, 0.59, 0.27, 0.07};

float ThumbTip2Power[] = {100.0, 99.76, 99.09, 98.01, 96.56, 94.8, 92.74, 90.43, 87.91, 85.22, 82.4, 79.47, 76.49, 73.5, 70.52, 67.6, 64.77, 62.08, 59.56, 57.25, 55.2, 53.43, 51.98, 50.9, 50.23};

float OtherTip2Power[] = {180.0, 179.39, 177.63, 174.83, 171.08, 166.48, 161.13, 155.13, 148.58, 141.58, 134.24, 126.64, 118.89, 111.1, 103.35, 95.76, 88.41, 81.41, 74.86, 68.86, 63.52, 58.91, 55.16, 52.36, 50.6};

float IndexPower2Open[] = {0.0, 0.51, 1.99, 4.37, 7.54, 11.44, 15.96, 21.04, 26.58, 32.5, 38.72, 45.14, 51.7, 58.29, 64.85, 71.27, 77.49, 83.41, 88.95, 94.03, 98.56, 102.45, 105.62, 108.0, 109.48};

float ThumbPower2Open[] = {50.0, 49.83, 49.36, 48.6, 47.59, 46.36, 44.91, 43.3, 41.54, 39.65, 37.68, 35.63, 33.54, 31.45, 29.36, 27.32, 25.34, 23.45, 21.69, 20.08, 18.64, 17.4, 16.39, 15.63, 15.16};

float OtherPower2Open[] = {50.0, 50.6, 52.36, 55.16, 58.91, 63.52, 68.86, 74.86, 81.41, 88.41, 95.76, 103.35, 111.1, 118.89, 126.64, 134.23, 141.58, 148.58, 155.13, 161.13, 166.47, 171.08, 174.83, 177.63, 179.39};

float IndexPower2Tip[] = {0.0, 0.07, 0.27, 0.59, 1.02, 1.56, 2.17, 2.86, 3.62, 4.43, 5.28, 6.15, 7.05, 7.94, 8.84, 9.71, 10.56, 11.37, 12.13, 12.82, 13.44, 13.97, 14.4, 14.72, 14.92};

float ThumbPower2Tip[] = {50.0, 50.23, 50.9, 51.98, 53.43, 55.2, 57.25, 59.56, 62.08, 64.77, 67.59, 70.52, 73.5, 76.49, 79.47, 82.4, 85.22, 87.91, 90.43, 92.74, 94.8, 96.56, 98.01, 99.09, 99.76};

float OtherPower2Tip[] = {50.0, 50.6, 52.36, 55.16, 58.91, 63.52, 68.86, 74.86, 81.41, 88.41, 95.76, 103.35, 111.1, 118.89, 126.64, 134.23, 141.58, 148.58, 155.13, 161.13, 166.47, 171.08, 174.83, 177.63, 179.39};

class MotorTrajectories{
	//0: Open, 1: Tip1, 2: Tip2, 3: Power, 4: Point
public:
	MotorTrajectories(int current_state, int number_states){
		changeCurrentState(current_state);
		if(currentState < number_states)
			changeTargetState(++currentState);
		else
			changeTargetState(--currentState);
		numberOfStates = number_states;
		nElements = 25;
	};

	void changeCurrentState(int state) {
		currentState = state;
	}

	int changeTargetState(int state) {
		if(state != currentState) {
			targetState = state;
			selectTrajectories();
			currentState = targetState;
		}else{
			return -1;
		}
		return 0;
	}

	void selectTrajectories(){
		if( currentState == 0 && targetState == 1) {
			IndexTraj = 120;
			ThumbTraj = 50;
			OtherTraj = 160;
		}else if( currentState == 0 && targetState == 3) {
			IndexTraj = 10;
			ThumbTraj = 50;
			OtherTraj = 25;
		}else if( currentState == 0 && targetState == 4) {
			IndexTraj = 120;
			ThumbTraj = 50;
			OtherTraj = 50;
		}else if( currentState == 1 && targetState == 0) {
			IndexTraj = 120;
			ThumbTraj = 160;
			OtherTraj = 160;
		}else if( currentState == 1 && targetState == 2) {
			IndexTraj = 20;
			ThumbTraj = 50;
			OtherTraj = 160;
		}else if( currentState == 1 && targetState == 3) {
			IndexTraj = 10;
			ThumbTraj = 50;
			OtherTraj = 25;
		}else if( currentState == 1 && targetState == 4) {
			IndexTraj = 120;
			ThumbTraj = 50;
			OtherTraj = 50;
		}else if( currentState == 2 && targetState == 0) {
			IndexTraj = 120;
			ThumbTraj = 160;
			OtherTraj = 160;
		}else if( currentState == 2 && targetState == 1) {
			IndexTraj = 120;
			ThumbTraj = 160;
			OtherTraj = 160;
		}else if( currentState == 2 && targetState == 3) {
			IndexTraj = 10;
			ThumbTraj = 50;
			OtherTraj = 25;
		}else if( currentState == 2 && targetState == 4) {
			IndexTraj = 120;
			ThumbTraj = 50;
			OtherTraj = 50;
		}else if( currentState == 3 && targetState == 0) {
			IndexTraj = 120;
			ThumbTraj = 160;
			OtherTraj = 160;
		}else if( currentState == 3 && targetState == 1) {
			IndexTraj = 120;
			ThumbTraj = 50;
			OtherTraj = 160;
		}else if( currentState == 3 && targetState == 4) {
			IndexTraj = 120;
			ThumbTraj = 50;
			OtherTraj = 50;
		}else if( currentState == 4 && targetState == 0) {
			IndexTraj = 120;
			ThumbTraj = 160;
			OtherTraj = 160;
		}else if( currentState == 4 && targetState == 1) {
			IndexTraj = 120;
			ThumbTraj = 50;
			OtherTraj = 160;
		}else if( currentState == 4 && targetState == 3) {
			IndexTraj = 10;
			ThumbTraj = 50;
			OtherTraj = 25;
		}else {
		}
	}



public:
int nElements;
int currentState;
int targetState;
int numberOfStates;

float IndexTraj;
float ThumbTraj;
float OtherTraj;


};

#endif /* TRAJECTORIES_HPP_ */
