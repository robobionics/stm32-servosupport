/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "servoHandler.hpp"
#include "trajectories.hpp"

/* USER CODE BEGIN Includes */
#include <vector>
// Grips:[0] -> Open, [1] -> Tip, [2] -> Power
//float IndexTraj[] = {90.0, 91.39, 92.79, 94.19, 95.59, 96.98, 98.37, 99.76, 101.15, 102.53, 103.9, 105.27, 106.63, 107.99, 109.34, 110.68, 112.01, 113.34, 114.65, 115.96, 117.25, 118.54, 119.81, 121.07, 122.32, 123.55, 124.78, 125.98, 127.18, 128.36, 129.52, 130.67, 131.8, 132.91, 134.01, 135.09, 136.15, 137.2, 138.22, 139.22, 140.21, 141.18, 142.12, 143.04, 143.95, 144.83, 145.69, 146.52, 147.34, 148.13, 148.9, 149.64, 150.36, 151.06, 151.73, 152.38, 153.0, 153.6, 154.17, 154.72, 155.24, 155.73, 156.2, 156.64, 157.06, 157.44, 157.81, 158.14, 158.45, 158.72, 158.98, 159.2, 159.4, 159.57, 159.71, 159.82, 159.9, 159.96, 159.99, 159.99, 159.97, 159.91, 159.83, 159.72, 159.58, 159.41, 159.22, 159.0, 158.75, 158.47, 158.16, 157.83, 157.47, 157.09, 156.68, 156.24, 155.77, 155.28, 154.76, 154.22, 153.65, 153.05, 152.43, 151.78, 151.11, 150.42, 149.7, 148.96, 148.19, 147.4, 146.59, 145.75, 144.9, 144.02, 143.12, 142.19, 141.25, 140.29, 139.3, 138.3, 137.28, 136.24, 135.18, 134.1, 133.0, 131.89, 130.76, 129.61, 128.45, 127.27, 126.08, 124.87, 123.65, 122.42, 121.17, 119.91, 118.64, 117.36, 116.06, 114.76, 113.44, 112.12, 110.79, 109.45, 108.1, 106.74, 105.38, 104.01, 102.64, 101.26, 99.87, 98.49, 97.09, 95.7, 94.3, 92.91, 91.51, 90.11};

int pointIdx = 0; // Initial Angle for testing purposes

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

uint16_t counterTIM3;
uint16_t counterTIM2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
servoHandler *servoManager;
MotorTrajectories TrajHandler(0, 4);
int targetState;
bool hasChangedState;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	vector<int> servo_pins(3);
	servo_pins[0] = GPIO_PIN_13; // 8192
	servo_pins[1] = GPIO_PIN_14; // 16384
	servo_pins[2] = GPIO_PIN_15; // 32768
	servoManager = new servoHandler(3, servo_pins, 0.5, 2.4, 2);
	targetState = 0;

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();

  /* USER CODE BEGIN 2 */

  HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_1);
  HAL_NVIC_SetPriority(TIM2_IRQn,0,0);
  HAL_NVIC_EnableIRQ(TIM2_IRQn);

  HAL_TIM_OC_Start_IT(&htim3, TIM_CHANNEL_1);
  HAL_NVIC_SetPriority(TIM3_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(TIM3_IRQn);
  __HAL_TIM_DISABLE_IT(&htim3,TIM_IT_CC1);

  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_RESET);

  /* USER CODE END 2 */
  bool isPressed = false;
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */
	  counterTIM3 = __HAL_TIM_GET_COUNTER(&htim3);
	  counterTIM2 = __HAL_TIM_GET_COUNTER(&htim2);
  /* USER CODE BEGIN 3 */
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) && !isPressed) {
			HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);

			if(targetState < 4) {
				targetState++;
			}else{
				targetState = 0;
			}


			HAL_Delay(10);
			isPressed = true;
			hasChangedState = true;
		}

		if(!HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) && isPressed){
			isPressed = false;
		}

		HAL_Delay(10);

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 8399;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 200;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 83;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 2400; // Dynamic period changed on TIM2 interruption event
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_OC_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12 | GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12 | GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
	static vector<float> angles(3);
	static servoData servo;
	static bool hasNewAngles = true;
	static int timeCounter = 0;


	if(htim->Instance == TIM2) { // 20ms
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_13, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);

		// check next desired state
		if(hasChangedState) {
			TrajHandler.changeTargetState(targetState);
			hasChangedState = false;
			pointIdx = 0;
		}

		// to avoid setting the same angles
		if(hasNewAngles) {
			angles[0] = TrajHandler.IndexTraj;//[pointIdx];
			angles[1] = TrajHandler.ThumbTraj;//[pointIdx];
			angles[2] = TrajHandler.OtherTraj;//[pointIdx];

			servoManager->setAngles(angles);
		}

		if(pointIdx < TrajHandler.nElements - 1) {
			pointIdx++;
		}else{
			pointIdx = 0; //Stay there!
		}

		servoManager->reset();
		servo = servoManager->popServo();

		HAL_TIM_OC_Start_IT(&htim3, TIM_CHANNEL_1);
		__HAL_TIM_SET_COUNTER(&htim2,1);
		__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_1,200);
		__HAL_TIM_ENABLE_IT(&htim2,TIM_IT_CC1);



		__HAL_TIM_ENABLE_IT(&htim3,TIM_IT_CC1); // Enable TIM3 signal
		__HAL_TIM_SET_COUNTER(&htim3,1);
		//__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1,2000);
		__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1,1000 * servo.time);
	}



	if(htim->Instance == TIM3) { // signal

		int nz = servoManager->nZeroTimeServos();

		for(int ss = 0; ss < nz; ss++) {
			HAL_GPIO_WritePin(GPIOD, servo.pin, GPIO_PIN_RESET);
			if(ss < nz - 1)
				servo = servoManager->popServo();
		}

		if(!servo.isLastServo) {
			servo = servoManager->popServo();
			// Set time for next interruption
			__HAL_TIM_SET_COUNTER(&htim3,1);
			__HAL_TIM_SET_COMPARE(&htim3,TIM_CHANNEL_1,1000 * servo.time);

			__HAL_TIM_ENABLE_IT(&htim3,TIM_IT_CC1); // Enable TIM3 signal
		}else {
			// disable timer
			__HAL_TIM_DISABLE_IT(&htim3,TIM_IT_CC1);
			HAL_TIM_OC_Stop(&htim3, TIM_CHANNEL_1);
		}

	}


}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
